extends Node2D

var title_screen = preload("res://Scenes/TitleScreen.tscn")
var game = preload("res://Scenes/Game.tscn")

var current_scene
var score = 0
var secret_ending = false

func _ready():
	current_scene = title_screen.instance()
	ChangeScene(title_screen)
	
func ChangeScene(scene):
	current_scene.queue_free()
	current_scene = scene.instance()
	add_child(current_scene)