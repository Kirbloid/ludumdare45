extends KinematicBody2D

signal drop_orb
signal spawning

export var nightmare = false
var held_by_player = false
var grouping = [ self ]
var growing = true

func _ready():
	connect("drop_orb", get_node("/root/Main/Game/Player"), "_on_DropOrb")
	connect("spawning", get_node("/root/Main/Game"), "_on_Spawning")
	scale = Vector2(0,0)

func _process(delta):
	$Sprite.z_index = position.y
	CheckSpawn()
	
func _physics_process(delta):
	if growing:
		var scale_step = Vector2(0.1,0.1)
		scale += scale_step
		if scale.x >= 1:
			scale = Vector2(1,1)
			growing = false
	
func CheckSpawn():
	if grouping.size() >= 4:
		var nightmare_count = 0
		for item in grouping:
			if item.nightmare:
				nightmare_count += 1
		emit_signal("spawning", position, nightmare_count)
		for item in grouping:
			item.queue_free()

func SendDropOrSignal():
	emit_signal("drop_orb")

func _on_DreamOrb_area_entered(area):
	if area.name != "Grab":
		grouping.append(area.get_parent())
		if grouping.size() >= 4:
			call_deferred("SendDropOrSignal")

func _on_DreamOrb_area_exited(area):
	if area.name != "Grab":
		grouping.erase(area.get_parent())
