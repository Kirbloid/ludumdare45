extends KinematicBody2D

export var SPEED = 25
export var damage = 10

var moving = false
var player
var velocity = Vector2()
var growing = true

func _ready():
	player = get_node("/root/Main/Game/Player")
	scale = Vector2(0,0)
	
func _process(delta):
	$Sprite.z_index = position.y

func _physics_process(delta):
	velocity = (player.position - position).normalized() * SPEED
	if (player.position - position).length() > 5 && moving:
		velocity = move_and_slide(velocity)
	if growing:
		var scale_step = Vector2(0.1,0.1)
		scale += scale_step
		if scale.x >= 1:
			scale = Vector2(1,1)
			growing = false

func _on_HurtPlayer_body_entered(body):
	if body.name == "Player":
		body.Attack(damage)
		queue_free()

func _on_UntilMoving_timeout():
	moving = true
	$HurtPlayer/CollisionShape2D.disabled = false
