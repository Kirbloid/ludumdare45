extends StaticBody2D

var growing = true

func _ready():
	$Sprite.z_index = position.y
	scale = Vector2(0,0)
	
func _physics_process(delta):
	if growing:
		var scale_step = Vector2(0.1,0.1)
		scale += scale_step
		if scale.x >= 1:
			scale = Vector2(1,1)
			growing = false

