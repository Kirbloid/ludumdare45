extends Node2D

const world_width = 640
const world_height = 360

var ending = preload("res://Scenes/EndScreen.tscn")

var dream_orb = preload("res://Scenes/DreamOrb.tscn")
var nightmare_orb = preload("res://Scenes/NightmareOrb.tscn")

var dream4a = preload("res://Scenes/Dreams/Dream4A.tscn")
var dream4b = preload("res://Scenes/Dreams/Dream4B.tscn")
var dream4c = preload("res://Scenes/Dreams/Dream4C.tscn")
var dream4d = preload("res://Scenes/Dreams/Dream4D.tscn")
var dream4e = preload("res://Scenes/Dreams/Dream4E.tscn")
var dream3a = preload("res://Scenes/Dreams/Dream3A.tscn")
var dream3b = preload("res://Scenes/Dreams/Dream3B.tscn")
var dream3c = preload("res://Scenes/Dreams/Dream3C.tscn")
var dream3d = preload("res://Scenes/Dreams/Dream3D.tscn")
var dream3e = preload("res://Scenes/Dreams/Dream3E.tscn")
var dream2a = preload("res://Scenes/Dreams/Dream2A.tscn")
var dream2b = preload("res://Scenes/Dreams/Dream2B.tscn")
var dream2c = preload("res://Scenes/Dreams/Dream2C.tscn")
var dream2d = preload("res://Scenes/Dreams/Dream2D.tscn")
var dream2e = preload("res://Scenes/Dreams/Dream2E.tscn")
var nightmare2a = preload("res://Scenes/Dreams/Nightmare2A.tscn")
var nightmare2b = preload("res://Scenes/Dreams/Nightmare2B.tscn")
var nightmare3a = preload("res://Scenes/Dreams/Nightmare3A.tscn")
var nightmare3b = preload("res://Scenes/Dreams/Nightmare3B.tscn")
var nightmare4a = preload("res://Scenes/Dreams/Nightmare4A.tscn")
var nightmare4b = preload("res://Scenes/Dreams/Nightmare4B.tscn")
var stainA = preload("res://Scenes/Dreams/StainA.tscn")
var stainB = preload("res://Scenes/Dreams/StainB.tscn")
var stainC = preload("res://Scenes/Dreams/StainC.tscn")
var stainD = preload("res://Scenes/Dreams/StainD.tscn")
var stainE = preload("res://Scenes/Dreams/StainE.tscn")

var rng = RandomNumberGenerator.new()

var main
var player
var dream_meter
var health_meter
var score_label

var beats_music
var dream_music
var nightmare_music

func _ready():
	rng.randomize()
	main = get_node("/root/Main")
	player = get_node("Player")
	dream_meter = get_node("UI/DreamJuice")
	health_meter = get_node("UI/HealthJuice")
	score_label = get_node("UI/ScoreCenter/ScoreHBox/Score")
	main.secret_ending = false
	beats_music = get_node("Beats")
	dream_music = get_node("DreamMusic")
	nightmare_music = get_node("NightmareMusic")
	
	dream_music.volume_db = -38
	nightmare_music.volume_db = -46
	beats_music.play()
	dream_music.play()
	nightmare_music.play()
	
	# spawn player
	var pos_x = rng.randi_range(0 + 64, world_width - 64)
	var pos_y = rng.randi_range(0 + 64, world_height - 64)
	player.position = Vector2(pos_x, pos_y)
	main.score = 0
	
func _process(delta):
	dream_meter.set_value(player.dream)
	health_meter.set_value(player.health)
	score_label.text = str(main.score)
	if player.dream <= 0 || player.health >= 100:
		main.secret_ending = true
		main.ChangeScene(ending)
	if Input.is_action_just_pressed("ui_cancel"):
		main.ChangeScene(ending)
	if Input.is_action_just_pressed("debug"):
		player.Attack(10)
	
func _on_Spawning(pos, nightmare_count):
	var dreamspawn
	match nightmare_count:
		0:
			match rng.randi_range(0, 4):
				0:
					dreamspawn = dream4a.instance()
				1:
					dreamspawn = dream4b.instance()
				2:
					dreamspawn = dream4c.instance()
				3:
					dreamspawn = dream4d.instance()
				4:
					dreamspawn = dream4e.instance()
			player.dream -= 20
			if dream_music.volume_db <= -12:
				dream_music.volume_db += 4
		1:
			match rng.randi_range(0, 4):
				0:
					dreamspawn = dream3a.instance()
				1:
					dreamspawn = dream3b.instance()
				2:
					dreamspawn = dream3c.instance()
				3:
					dreamspawn = dream3d.instance()
				4:
					dreamspawn = dream3e.instance()
			player.dream -= 12
			if dream_music.volume_db <= -12:
				dream_music.volume_db += 4
		2:
			match rng.randi_range(0, 4):
				0:
					dreamspawn = dream2a.instance()
				1:
					dreamspawn = dream2b.instance()
				2:
					dreamspawn = dream2c.instance()
				3:
					dreamspawn = dream2d.instance()
				4:
					dreamspawn = dream2e.instance()
			dreamspawn.position = pos
			add_child(dreamspawn)
			match rng.randi_range(0, 1):
				0:
					dreamspawn = nightmare2a.instance()
				1:
					dreamspawn = nightmare2b.instance()
			player.dream += 8
			if dream_music.volume_db <= -12:
				dream_music.volume_db += 2
			if nightmare_music.volume_db <= -12:
				nightmare_music.volume_db += 2
		3:
			match rng.randi_range(0, 1):
				0:
					dreamspawn = nightmare3a.instance()
				1:
					dreamspawn = nightmare3b.instance()
			player.dream += 20
			if nightmare_music.volume_db <= -12:
				nightmare_music.volume_db += 4
		4:
			match rng.randi_range(0, 1):
				0:
					dreamspawn = nightmare4a.instance()
				1:
					dreamspawn = nightmare4b.instance()
			player.dream += 35
			if nightmare_music.volume_db <= -12:
				nightmare_music.volume_db += 4
	if player.dream > 100:
		player.dream = 100
	dreamspawn.position = pos
	add_child(dreamspawn)
	if nightmare_count > 2:
		var stainspawn
		match rng.randi_range(0, 4):
			0:
				stainspawn = stainA.instance()
			1:
				stainspawn = stainB.instance()
			2:
				stainspawn = stainC.instance()
			3:
				stainspawn = stainD.instance()
			4:
				stainspawn = stainE.instance()
		stainspawn.position = pos
		add_child(stainspawn)
	main.score += 250 - ( 50 * nightmare_count )
	beats_music.volume_db -= 1.5

func _on_SpawnOrbTimer_timeout():
	var pos_x = rng.randi_range(0 + 32, world_width - 32)
	var pos_y = rng.randi_range(0 + 32, world_height - 32)
	var orbspawn
	match rng.randi_range(0, 4):
		0,1,2:
			orbspawn = dream_orb.instance()
		3,4:
			orbspawn = nightmare_orb.instance()
	orbspawn.position = Vector2(pos_x, pos_y)
	add_child(orbspawn)

func _on_Beats_finished():
	beats_music.stop()
	dream_music.stop()
	nightmare_music.stop()
	beats_music.play(0.0)
	dream_music.play(0.0)
	nightmare_music.play(0.0)
