extends Control

var main

func _ready():
	main = get_node("/root/Main")
	$CenterContainer/VBoxContainer/Score.text = str(main.score)
	
	if main.score > 2500:
		if main.secret_ending:
			$Ending4.visible = true
		else:
			$Ending3.visible = true
	elif main.score >= 1250:
		$Ending2.visible = true
	
func _process(delta):
	if Input.is_action_just_pressed("grab"):
		main.ChangeScene(main.title_screen)
	elif Input.is_action_just_pressed("quickstart"):
		main.ChangeScene(main.game)
