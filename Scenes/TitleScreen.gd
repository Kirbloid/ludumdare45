extends Control

var main
var intro = 0

func _ready():
	main = get_node("/root/Main")
	
func _process(delta):
	if Input.is_action_just_pressed("grab"):
		if intro == 0:
			$Melody.stop()
			$Beats.play()
			$TitleScreen.visible = false
			intro += 1
		elif intro == 1:
			$IntroScreen1.visible = false
			intro += 1
		elif intro == 2:
			$IntroScreen2.visible = false
			intro += 1
		elif intro == 3:
			$IntroScreen3.visible = false
			intro += 1
		elif intro == 4:
			main.ChangeScene(main.game)
	elif Input.is_action_just_pressed("quickstart"):
		main.ChangeScene(main.game)