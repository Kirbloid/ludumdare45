extends KinematicBody2D

const SPEED = 15
const MAXACCEL = 50
const MAXDREAM = 100
const MAXHEALTH = 100

var dream = 100
var health = 0
var velocity = Vector2()
var acceleration_velocity = Vector2()
var looking_left = false

var orb_pickup
var holding_orb

func _ready():
	pass

func _process(delta):
	GetMovement()
	GetGrab()
	$Sprite.z_index = position.y
		
func _physics_process(delta):
	move_and_slide(velocity + acceleration_velocity)

func GetMovement():
	velocity = Vector2()
	var moving_x = false
	var moving_y = false
	
	if Input.is_action_pressed("ui_right"):
		moving_x = true
		velocity.x += SPEED
		acceleration_velocity.x = clamp(acceleration_velocity.x + 1, 0, MAXACCEL)
		if (looking_left && holding_orb == null):
			apply_scale(Vector2(-1, 1))
			looking_left = false
	if Input.is_action_pressed("ui_left"):
		moving_x = true
		velocity.x -= SPEED
		acceleration_velocity.x = clamp(acceleration_velocity.x - 1, -MAXACCEL, 0)
		if (!looking_left && holding_orb == null):
			apply_scale(Vector2(-1, 1))
			looking_left = true
	if Input.is_action_pressed("ui_down"):
		moving_y = true
		velocity.y += SPEED
		acceleration_velocity.y = clamp(acceleration_velocity.y + 1, 0, MAXACCEL)
	if Input.is_action_pressed("ui_up"):
		moving_y = true
		velocity.y -= SPEED
		acceleration_velocity.y = clamp(acceleration_velocity.y - 1, -MAXACCEL, 0)
		
	if moving_x == false:
		if (acceleration_velocity.x < 0):
			acceleration_velocity.x = clamp(acceleration_velocity.x + 2, -MAXACCEL, 0)
		if (acceleration_velocity.x > 0):
			acceleration_velocity.x = clamp(acceleration_velocity.x - 2, 0, MAXACCEL)
			
	if moving_y == false:
		if (acceleration_velocity.y < 0):
			acceleration_velocity.y = clamp(acceleration_velocity.y + 2, -MAXACCEL, 0)
		if (acceleration_velocity.y > 0):
			acceleration_velocity.y = clamp(acceleration_velocity.y - 2, 0, MAXACCEL)
		
	if holding_orb == null:
		velocity *= 4
		if orb_pickup != null:
			$Sprite.animation = "can_grab"
		else:
			$Sprite.animation = "default"
		
func GetGrab():
	if Input.is_action_just_pressed("grab") && orb_pickup != null:
		$Grab.set_collision_mask(0)
		holding_orb = orb_pickup
		orb_pickup = null
		holding_orb.get_parent().remove_child(holding_orb)
		$Grab.add_child(holding_orb)
		holding_orb.position = Vector2()
		holding_orb.held_by_player = true
		$OrbCollisionShape.disabled = false
		holding_orb.get_node("CollisionShape2D").disabled = true
		$Sprite.animation = "grabbing"
	elif Input.is_action_just_pressed("grab") && holding_orb != null:
		DropOrb()
		$Sprite.animation = "default"
		
func DropOrb():
	holding_orb.held_by_player = false
	$Grab.remove_child(holding_orb)
	get_parent().add_child(holding_orb)
	holding_orb.position = $Grab.get_global_transform().get_origin()
	holding_orb.get_node("CollisionShape2D").disabled = false
	holding_orb = null
	$Grab.set_collision_mask(2)
	$OrbCollisionShape.disabled = true

func Attack(damage):
	health += damage

func _on_Grab_area_entered(area):
	orb_pickup = area.get_parent()

func _on_Grab_area_exited(area):
	if orb_pickup == area.get_parent():
		orb_pickup = null

func _on_DreamTimer_timeout():
	dream -= 1

func _on_DropOrb():
	if holding_orb != null:
		DropOrb()